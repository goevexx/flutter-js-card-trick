import 'package:dartz/dartz.dart' as dartz;
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/core/deck_provider.dart';
import 'package:js_card_trick/application/trick/trick_provider.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/presentation/trick/widgets/card_selection.dart';
import 'package:shake/shake.dart';

class TrickForm extends ConsumerStatefulWidget {
  const TrickForm({
    Key? key,
  }) : super(key: key);

  @override
  _TrickFormState createState() => _TrickFormState();
}

class _TrickFormState extends ConsumerState<TrickForm> {
  final GlobalKey<FormState> _form = GlobalKey();

  @override
  void initState() {
    super.initState();
    ShakeDetector.autoStart(
      onPhoneShake: () {
        _form.currentState!.reset();
        ref.read(deckProvider.notifier).reset();
        ref.read(trickProvider.notifier).reset();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final trickNotifier = ref.read(trickProvider.notifier);
    return Form(
      key: _form,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          FlexCardSelectionP8(
            '1. Karte auswählen',
            onChange: trickNotifier.firstCardChanged,
          ),
          FlexCardSelectionP8(
            '2. Karte auswählen',
            onChange: trickNotifier.secondCardChanged,
          ),
          FlexCardSelectionP8(
            '3. Karte auswählen',
            onChange: trickNotifier.thirdCardChanged,
          ),
          FlexCardSelectionP8(
            '4. Karte auswählen',
            onChange: trickNotifier.fourthCardChanged,
          ),
        ],
      ),
    );
  }
}

class FlexCardSelectionP8 extends ConsumerWidget {
  final String placeholderText;
  final void Function(dartz.Option<Cards>)? onChange;
  const FlexCardSelectionP8(
    this.placeholderText, {
    Key? key,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final deckNotifier = ref.read(deckProvider.notifier);
    return Flexible(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CardSelection(
          context,
          deckNotifier,
          text: placeholderText,
          onChange: onChange,
        ),
      ),
    );
  }
}
