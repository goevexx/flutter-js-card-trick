import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/core/card_provider.dart';
import 'package:js_card_trick/domain/core/card_image.dart';
import 'package:js_card_trick/domain/core/cards.dart';

class CardWidget extends StatelessWidget {
  final String cardFile;
  const CardWidget(this.cardFile, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, child) {
        final cardState = ref.watch(cardProvider(cardFile));
        return cardState.state.when(
          initial: () => Container(),
          loading: () => Container(),
          success: (card) {
            return cardFile == Cards.cBack().filename
                ? CardContent(
                    card: card,
                  )
                : Hero(
                    tag: cardFile,
                    child: CardContent(
                      card: card,
                    ),
                  );
          },
          error: (f) => Text('ERROR: $f'),
        );
      },
    );
  }
}

class CardContent extends StatelessWidget {
  final CardImage card;
  const CardContent({
    Key? key,
    required this.card,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            offset: Offset(5, 5),
            blurRadius: 10,
          )
        ],
        color: Colors.white,
      ),
      child: card.image,
    );
  }
}
