import 'package:auto_route/auto_route.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:js_card_trick/application/core/deck_notifier.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/presentation/routes/app_router.gr.dart';
import 'package:js_card_trick/presentation/trick/widgets/card_placeholder.dart';
import 'package:js_card_trick/presentation/trick/widgets/card_widget.dart';

class CardSelection extends FormField<Option<Cards>> {
  final BuildContext context;
  final Function(Option<Cards>)? onChange;
  final String? text;
  final DeckNotifier deckNotifier;
  CardSelection(this.context, this.deckNotifier, {this.text, this.onChange})
      : super(
          builder: (FormFieldState<Option<Cards>> state) => GestureDetector(
            onTap: () => context.pushRoute(CardChooserRoute()).then(
              (cardOrNull) {
                final card = cardOrNull as Cards?;
                optionOf(card).fold(
                  () => null,
                  (card) {
                    state.value?.fold(
                      () => null,
                      (card) => deckNotifier.cardUntaken(card),
                    );
                    if (onChange != null) {
                      onChange(optionOf(card));
                    }
                    state.didChange(optionOf(card));
                    deckNotifier.cardTaken(card);
                  },
                );
              },
            ),
            child: state.value!.fold(
              () => CardPlaceholder(
                text: text,
              ),
              (card) => CardWidget(card.filename),
            ),
          ),
          initialValue: none(),
          validator: (cardOption) {
            if (cardOption != null) {
              if (cardOption.isSome()) {
                return null;
              }
            }
            return 'Keine Karte ausgewählt';
          },
        );
}
