import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/presentation/trick/widgets/card_widget.dart';

class CardPlaceholder extends StatelessWidget {
  final String? text;
  const CardPlaceholder({Key? key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Container(
        color: Colors.white,
        child: DottedBorder(
          borderType: BorderType.RRect,
          dashPattern: [6, 3, 2, 3],
          strokeCap: StrokeCap.round,
          radius: Radius.circular(5),
          strokeWidth: 2,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Opacity(opacity: 0, child: CardWidget(Cards.cBack().filename)),
              text != null
                  ? Text(
                      text!,
                      textAlign: TextAlign.center,
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
