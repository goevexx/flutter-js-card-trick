import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/core/deck_provider.dart';
import 'package:js_card_trick/application/trick/trick_provider.dart';
import 'package:js_card_trick/application/trick/trick_state.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/presentation/trick/widgets/card_widget.dart';

class TrickResult extends ConsumerStatefulWidget {
  const TrickResult({
    Key? key,
  }) : super(key: key);

  @override
  _TrickResultState createState() => _TrickResultState();
}

class _TrickResultState extends ConsumerState<TrickResult>
    with SingleTickerProviderStateMixin {
  late final AnimationController _flipAnimationController;
  late final Animation<double> _flipAnimation;

  @override
  void initState() {
    super.initState();
    _flipAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 150));
    _flipAnimation =
        Tween<double>(begin: 1, end: 0).animate(_flipAnimationController)
          ..addListener(() {
            setState(() {});
          });
  }

  @override
  void dispose() {
    _flipAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ref.listen<TrickState>(trickProvider, (previous, next) {
      next.resultOption.fold(() => _flipAnimationController.reverse(),
          (_) => _flipAnimationController.forward());
    });
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Transform(
        transform: Matrix4.identity()
          ..setEntry(3, 2, 0.002)
          ..rotateY(pi * _flipAnimation.value),
        alignment: Alignment.center,
        child: Consumer(
          builder: (context, ref, child) {
            return ref.watch(trickProvider).resultOption.fold(
                  () => GestureDetector(
                    onTap: () => ref
                        .read(trickProvider.notifier)
                        .evaluateResult(ref.read(deckProvider).cards),
                    child: child!,
                  ),
                  (resultCard) => _flipAnimation.value <= 0.5
                      ? CardWidget(resultCard.filename)
                      : child!,
                );
          },
          child: CardWidget(Cards.cBack().filename),
        ),
      ),
    );
  }
}
