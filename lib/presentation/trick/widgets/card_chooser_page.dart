import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/core/deck_provider.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/presentation/core/background_image.dart';
import 'package:js_card_trick/presentation/trick/widgets/card_widget.dart';
import 'package:kt_dart/kt.dart';

class CardChooserPage extends ConsumerWidget {
  const CardChooserPage({Key? key}) : super(key: key);

  Iterable<WrappedCardWidget> _buildCards(
      KtList<Cards> cards, double width) sync* {
    for (var card in cards.asList()) {
      yield WrappedCardWidget(
        card: card,
        width: width,
      );
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final cards = ref.read(deckProvider).cards;
    return Scaffold(
      body: Stack(
        children: [
          BackgroundImage(),
          SafeArea(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: LayoutBuilder(builder: (context, constraints) {
                  final runsPerRow = 4;
                  final wrapNetQuarterWidth =
                      (constraints.maxWidth - 12 * (runsPerRow - 1)) /
                          runsPerRow;
                  return Wrap(
                    spacing: 12,
                    runSpacing: 12,
                    children: _buildCards(
                      cards,
                      wrapNetQuarterWidth,
                    ).toList(),
                  );
                }),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class WrappedCardWidget extends StatelessWidget {
  final Cards card;
  final double width;
  const WrappedCardWidget({Key? key, required this.card, required this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      child: card.taken
          ? CardWidget(Cards.cBack().filename)
          : GestureDetector(
              onTap: () {
                context.popRoute(card);
              },
              child: CardWidget(
                card.filename,
              ),
            ),
    );
  }
}
