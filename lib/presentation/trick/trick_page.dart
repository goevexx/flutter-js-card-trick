import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/trick/trick_provider.dart';
import 'package:js_card_trick/application/trick/trick_state.dart';
import 'package:js_card_trick/presentation/core/background_image.dart';
import 'package:js_card_trick/presentation/trick/widgets/card_result.dart';
import 'package:js_card_trick/presentation/trick/widgets/trick_form.dart';

class TrickPage extends ConsumerStatefulWidget {
  const TrickPage({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState<TrickPage> createState() => _TrickPageState();
}

class _TrickPageState extends ConsumerState<TrickPage> {
  bool showResult = false;

  @override
  Widget build(BuildContext context) {
    ref.listen<TrickState>(trickProvider, (_, trickState) {
      final validTrick = trickState.cards.isValid();

      if (validTrick && !showResult) {
        Future.delayed(Duration(seconds: 5), () {
          setState(() {
            showResult = true;
          });
        });
      } else if (!validTrick) {
        Future(() {
          setState(() {
            showResult = false;
          });
        });
      }
    });
    return Scaffold(
      body: Stack(
        children: [
          BackgroundImage(),
          Center(
            child: SingleChildScrollView(
              child: SafeArea(
                child: Padding(
                  padding: EdgeInsets.all(8),
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TrickForm(),
                        AnimatedSwitcher(
                          duration: Duration(milliseconds: 500),
                          transitionBuilder: (child, animation) {
                            return FadeTransition(
                              opacity: animation,
                              child: child,
                            );
                          },
                          child: showResult ? TrickResult() : SizedBox(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
