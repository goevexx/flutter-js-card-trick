import 'package:flutter/material.dart';

class BackgroundImage extends StatelessWidget {
  const BackgroundImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
        child: Image.asset(
      'assets/images/backgrounds/table.jpg',
      fit: BoxFit.fill,
    ));
  }
}
