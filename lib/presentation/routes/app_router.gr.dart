// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i4;

import '../../domain/core/cards.dart' as _i5;
import '../trick/trick_page.dart' as _i1;
import '../trick/widgets/card_chooser_page.dart' as _i2;

class AppRouter extends _i3.RootStackRouter {
  AppRouter([_i4.GlobalKey<_i4.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    TrickRoute.name: (routeData) {
      return _i3.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.TrickPage());
    },
    CardChooserRoute.name: (routeData) {
      return _i3.MaterialPageX<_i5.Cards>(
          routeData: routeData, child: const _i2.CardChooserPage());
    }
  };

  @override
  List<_i3.RouteConfig> get routes => [
        _i3.RouteConfig(TrickRoute.name, path: '/'),
        _i3.RouteConfig(CardChooserRoute.name, path: '/choose-card')
      ];
}

/// generated route for
/// [_i1.TrickPage]
class TrickRoute extends _i3.PageRouteInfo<void> {
  const TrickRoute() : super(TrickRoute.name, path: '/');

  static const String name = 'TrickRoute';
}

/// generated route for
/// [_i2.CardChooserPage]
class CardChooserRoute extends _i3.PageRouteInfo<void> {
  const CardChooserRoute() : super(CardChooserRoute.name, path: '/choose-card');

  static const String name = 'CardChooserRoute';
}
