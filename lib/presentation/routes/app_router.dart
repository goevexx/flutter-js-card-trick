import 'package:auto_route/annotations.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/presentation/trick/trick_page.dart';
import 'package:js_card_trick/presentation/trick/widgets/card_chooser_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(
      page: TrickPage,
      initial: true,
      path: '/',
    ),
    AutoRoute<Cards>(path: '/choose-card', page: CardChooserPage),
  ],
)
class $AppRouter {}
