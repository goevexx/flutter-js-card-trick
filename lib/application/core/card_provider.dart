import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/core/card_notifier.dart';
import 'package:js_card_trick/application/core/card_state.dart';
import 'package:js_card_trick/infrastructure/core/injectable_modules.dart';

final cardProvider =
    StateNotifierProvider.family<CardNotifier, CardState, String>(
  (ref, card) => CardNotifier(
    ref.watch(cardFacadeProvider),
    card,
  ),
);
