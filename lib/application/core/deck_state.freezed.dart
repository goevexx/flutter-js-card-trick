// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'deck_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$DeckStateTearOff {
  const _$DeckStateTearOff();

  _DeckState call({required KtList<Cards> cards}) {
    return _DeckState(
      cards: cards,
    );
  }
}

/// @nodoc
const $DeckState = _$DeckStateTearOff();

/// @nodoc
mixin _$DeckState {
  KtList<Cards> get cards => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DeckStateCopyWith<DeckState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeckStateCopyWith<$Res> {
  factory $DeckStateCopyWith(DeckState value, $Res Function(DeckState) then) =
      _$DeckStateCopyWithImpl<$Res>;
  $Res call({KtList<Cards> cards});
}

/// @nodoc
class _$DeckStateCopyWithImpl<$Res> implements $DeckStateCopyWith<$Res> {
  _$DeckStateCopyWithImpl(this._value, this._then);

  final DeckState _value;
  // ignore: unused_field
  final $Res Function(DeckState) _then;

  @override
  $Res call({
    Object? cards = freezed,
  }) {
    return _then(_value.copyWith(
      cards: cards == freezed
          ? _value.cards
          : cards // ignore: cast_nullable_to_non_nullable
              as KtList<Cards>,
    ));
  }
}

/// @nodoc
abstract class _$DeckStateCopyWith<$Res> implements $DeckStateCopyWith<$Res> {
  factory _$DeckStateCopyWith(
          _DeckState value, $Res Function(_DeckState) then) =
      __$DeckStateCopyWithImpl<$Res>;
  @override
  $Res call({KtList<Cards> cards});
}

/// @nodoc
class __$DeckStateCopyWithImpl<$Res> extends _$DeckStateCopyWithImpl<$Res>
    implements _$DeckStateCopyWith<$Res> {
  __$DeckStateCopyWithImpl(_DeckState _value, $Res Function(_DeckState) _then)
      : super(_value, (v) => _then(v as _DeckState));

  @override
  _DeckState get _value => super._value as _DeckState;

  @override
  $Res call({
    Object? cards = freezed,
  }) {
    return _then(_DeckState(
      cards: cards == freezed
          ? _value.cards
          : cards // ignore: cast_nullable_to_non_nullable
              as KtList<Cards>,
    ));
  }
}

/// @nodoc

class _$_DeckState implements _DeckState {
  const _$_DeckState({required this.cards});

  @override
  final KtList<Cards> cards;

  @override
  String toString() {
    return 'DeckState(cards: $cards)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _DeckState &&
            const DeepCollectionEquality().equals(other.cards, cards));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(cards));

  @JsonKey(ignore: true)
  @override
  _$DeckStateCopyWith<_DeckState> get copyWith =>
      __$DeckStateCopyWithImpl<_DeckState>(this, _$identity);
}

abstract class _DeckState implements DeckState {
  const factory _DeckState({required KtList<Cards> cards}) = _$_DeckState;

  @override
  KtList<Cards> get cards;
  @override
  @JsonKey(ignore: true)
  _$DeckStateCopyWith<_DeckState> get copyWith =>
      throw _privateConstructorUsedError;
}
