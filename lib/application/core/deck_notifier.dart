import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/core/deck_state.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:kt_dart/kt.dart';

class DeckNotifier extends StateNotifier<DeckState> {
  DeckNotifier() : super(DeckState.initial());

  void cardTaken(Cards card) {
    final newCards = state.cards.asList().toList();
    final cardIndex =
        state.cards.indexOfFirst((sCard) => sCard.filename == card.filename);
    newCards[cardIndex] = card.copyWith(taken: true);
    state = state.copyWith(
      cards: newCards.toImmutableList(),
    );
  }

  void cardUntaken(Cards card) {
    final newCards = state.cards.asList().toList();
    final cardIndex =
        state.cards.indexOfFirst((sCard) => sCard.filename == card.filename);
    newCards[cardIndex] = card.copyWith(taken: false);
    state = state.copyWith(
      cards: newCards.toImmutableList(),
    );
  }

  void reset() {
    state = DeckState.initial();
  }
}
