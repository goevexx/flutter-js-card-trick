import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:kt_dart/kt.dart';

part 'deck_state.freezed.dart';

@freezed
class DeckState with _$DeckState {
  const factory DeckState({required KtList<Cards> cards}) = _DeckState;

  factory DeckState.initial() => DeckState(
          cards: [
        Cards.c2OfClubs(),
        Cards.c2OfDiamonds(),
        Cards.c2OfHearts(),
        Cards.c2OfSpades(),
        Cards.c3OfClubs(),
        Cards.c3OfDiamonds(),
        Cards.c3OfHearts(),
        Cards.c3OfSpades(),
        Cards.c4OfClubs(),
        Cards.c4OfDiamonds(),
        Cards.c4OfHearts(),
        Cards.c4OfSpades(),
        Cards.c5OfClubs(),
        Cards.c5OfDiamonds(),
        Cards.c5OfHearts(),
        Cards.c5OfSpades(),
        Cards.c6OfClubs(),
        Cards.c6OfDiamonds(),
        Cards.c6OfHearts(),
        Cards.c6OfSpades(),
        Cards.c7OfClubs(),
        Cards.c7OfDiamonds(),
        Cards.c7OfHearts(),
        Cards.c7OfSpades(),
        Cards.c8OfClubs(),
        Cards.c8OfDiamonds(),
        Cards.c8OfHearts(),
        Cards.c8OfSpades(),
        Cards.c9OfClubs(),
        Cards.c9OfDiamonds(),
        Cards.c9OfHearts(),
        Cards.c9OfSpades(),
        Cards.c10OfClubs(),
        Cards.c10OfDiamonds(),
        Cards.c10OfHearts(),
        Cards.c10OfSpades(),
        Cards.cAceOfClubs(),
        Cards.cAceOfDiamonds(),
        Cards.cAceOfHearts(),
        Cards.cAceOfSpades(),
        Cards.cJackOfClubs(),
        Cards.cJackOfDiamonds(),
        Cards.cJackOfHearts(),
        Cards.cJackOfSpades(),
        Cards.cKingOfClubs(),
        Cards.cKingOfDiamonds(),
        Cards.cKingOfHearts(),
        Cards.cKingOfSpades(),
        Cards.cQueenOfClubs(),
        Cards.cQueenOfDiamonds(),
        Cards.cQueenOfHearts(),
        Cards.cQueenOfSpades(),
      ].toImmutableList());
}
