import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:js_card_trick/domain/core/card_failure.dart';
import 'package:js_card_trick/domain/core/card_image.dart';

part 'card_state.freezed.dart';

@freezed
class LoadingState with _$LoadingState {
  const factory LoadingState.initial() = _Initial;
  const factory LoadingState.loading() = _Loading;
  const factory LoadingState.success(CardImage card) = _Success;
  const factory LoadingState.error(CardFailure f) = _Error;
}

@freezed
class CardState with _$CardState {
  const factory CardState({
    required LoadingState state,
    required bool covered,
  }) = _CardState;

  factory CardState.initial() => CardState(
        state: LoadingState.initial(),
        covered: false,
      );
}
