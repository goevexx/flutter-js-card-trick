import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/core/card_state.dart';
import 'package:js_card_trick/domain/core/i_card_facade.dart';

class CardNotifier extends StateNotifier<CardState> {
  final ICardFacade _cardFacade;
  CardNotifier(this._cardFacade, String filename) : super(CardState.initial()) {
    loadImage(filename);
  }

  Future<void> loadImage(String filename) async {
    state = state.copyWith(state: LoadingState.loading());
    final card = await _cardFacade.getCard(filename);
    LoadingState loadingState = card.fold(
      (f) => LoadingState.error(f),
      (card) => LoadingState.success(card),
    );
    state = state.copyWith(state: loadingState);
  }
}
