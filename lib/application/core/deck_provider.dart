import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/core/deck_notifier.dart';
import 'package:js_card_trick/application/core/deck_state.dart';

final deckProvider = StateNotifierProvider<DeckNotifier, DeckState>((ref) {
  return DeckNotifier();
});
