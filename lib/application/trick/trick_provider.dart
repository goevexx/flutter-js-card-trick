import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/trick/trick_notifier.dart';
import 'package:js_card_trick/application/trick/trick_state.dart';

final trickProvider = StateNotifierProvider<TrickNotifier, TrickState>((ref) {
  return TrickNotifier();
});
