import 'package:dartz/dartz.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/application/trick/trick_state.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/domain/trick/value_objects.dart';
import 'package:kt_dart/kt.dart';

class TrickNotifier extends StateNotifier<TrickState> {
  TrickNotifier() : super(TrickState.intial());

  KtList<Option<Cards>> _changeCardAt(
      KtList<Option<Cards>> cards, int index, Option<Cards> newCard) {
    var newCards = cards.toMutableList();
    newCards[index] = newCard;
    return newCards.toList();
  }

  void firstCardChanged(Option<Cards> card) {
    var cards = state.cards.getRaw();
    state = state.copyWith(
      cards: CardList4(_changeCardAt(cards, 0, card)),
      resultOption: none(),
    );
  }

  void secondCardChanged(Option<Cards> card) {
    var cards = state.cards.getRaw();
    state = state.copyWith(
      cards: CardList4(_changeCardAt(cards, 1, card)),
      resultOption: none(),
    );
  }

  void thirdCardChanged(Option<Cards> card) {
    var cards = state.cards.getRaw();
    state = state.copyWith(
      cards: CardList4(_changeCardAt(cards, 2, card)),
      resultOption: none(),
    );
  }

  void fourthCardChanged(Option<Cards> card) {
    var cards = state.cards.getRaw();
    state = state.copyWith(
      cards: CardList4(_changeCardAt(cards, 3, card)),
      resultOption: none(),
    );
  }

  void reset() {
    state = TrickState.intial();
  }

  void evaluateResult(KtList<Cards> deck) {
    final resultCard = _theMagicTrick(deck, state.cards);
    state = state.copyWith(resultOption: resultCard);
  }

  Option<Cards> _theMagicTrick(KtList<Cards> deck, CardList4 cards) {
    if (cards.isValid()) {
      final cards = state.cards.getOrCrash();
      return _readMind(
        deck.map((card) => optionOf(card)),
        cards[0].fold(() => null, id)!,
        cards[1].fold(() => null, id)!,
        cards[2].fold(() => null, id)!,
        cards[3].fold(() => null, id)!,
      );
    }
    return none();
  }

  Option<Cards> _readMind(
      KtList<Option<Cards>> deck, Cards cardOne, cardTwo, cardThree, cardFour) {
    final resultSymbol = cardOne.symbol;
    final offset = _orderOf(cardTwo, cardThree, cardFour);
    var resultValue = offset + cardOne.value.index;
    if (resultValue > Values.values.last.index) {
      resultValue = resultValue - (Values.values.last.index + 1);
    }
    final resultCardOption = deck.asList().firstWhere(
          (cardOption) => cardOption.fold(
            () => false,
            (card) =>
                card.symbol == resultSymbol && card.value.index == resultValue,
          ),
        );
    return resultCardOption;
  }

  int _orderOf(Cards cardTwo, cardThree, cardFour) {
    if (cardTwo.compare(cardThree)) {
      // 2 > 3
      if (cardTwo.compare(cardFour)) {
        //card 2 is largest
        if (cardThree.compare(cardFour)) {
          // 2 > 3 > 4
          return 6;
        } else {
          // 2 > 4 > 3
          return 5;
        }
      } else {
        // 4 > 2 > 3
        return 3;
      }
    } else {
      // 3 > 2
      if (cardThree.compare(cardFour)) {
        // card 3 is largest
        if (cardTwo.compare(cardFour)) {
          // 3 > 2 > 4
          return 4;
        } else {
          // 3 > 4 > 2
          return 2;
        }
      } else {
        // 4 > 3 > 2
        return 1;
      }
    }
  }
}
