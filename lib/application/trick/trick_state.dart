import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/domain/trick/value_objects.dart';

part 'trick_state.freezed.dart';

@freezed
class TrickState with _$TrickState {
  const factory TrickState({
    required CardList4 cards,
    required Option<Cards> resultOption,
  }) = _TrickState;

  factory TrickState.intial() => TrickState(
        cards: CardList4.empty(),
        resultOption: none(),
      );
}
