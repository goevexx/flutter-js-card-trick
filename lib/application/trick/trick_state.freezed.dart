// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'trick_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$TrickStateTearOff {
  const _$TrickStateTearOff();

  _TrickState call(
      {required CardList4 cards, required Option<Cards> resultOption}) {
    return _TrickState(
      cards: cards,
      resultOption: resultOption,
    );
  }
}

/// @nodoc
const $TrickState = _$TrickStateTearOff();

/// @nodoc
mixin _$TrickState {
  CardList4 get cards => throw _privateConstructorUsedError;
  Option<Cards> get resultOption => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TrickStateCopyWith<TrickState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrickStateCopyWith<$Res> {
  factory $TrickStateCopyWith(
          TrickState value, $Res Function(TrickState) then) =
      _$TrickStateCopyWithImpl<$Res>;
  $Res call({CardList4 cards, Option<Cards> resultOption});
}

/// @nodoc
class _$TrickStateCopyWithImpl<$Res> implements $TrickStateCopyWith<$Res> {
  _$TrickStateCopyWithImpl(this._value, this._then);

  final TrickState _value;
  // ignore: unused_field
  final $Res Function(TrickState) _then;

  @override
  $Res call({
    Object? cards = freezed,
    Object? resultOption = freezed,
  }) {
    return _then(_value.copyWith(
      cards: cards == freezed
          ? _value.cards
          : cards // ignore: cast_nullable_to_non_nullable
              as CardList4,
      resultOption: resultOption == freezed
          ? _value.resultOption
          : resultOption // ignore: cast_nullable_to_non_nullable
              as Option<Cards>,
    ));
  }
}

/// @nodoc
abstract class _$TrickStateCopyWith<$Res> implements $TrickStateCopyWith<$Res> {
  factory _$TrickStateCopyWith(
          _TrickState value, $Res Function(_TrickState) then) =
      __$TrickStateCopyWithImpl<$Res>;
  @override
  $Res call({CardList4 cards, Option<Cards> resultOption});
}

/// @nodoc
class __$TrickStateCopyWithImpl<$Res> extends _$TrickStateCopyWithImpl<$Res>
    implements _$TrickStateCopyWith<$Res> {
  __$TrickStateCopyWithImpl(
      _TrickState _value, $Res Function(_TrickState) _then)
      : super(_value, (v) => _then(v as _TrickState));

  @override
  _TrickState get _value => super._value as _TrickState;

  @override
  $Res call({
    Object? cards = freezed,
    Object? resultOption = freezed,
  }) {
    return _then(_TrickState(
      cards: cards == freezed
          ? _value.cards
          : cards // ignore: cast_nullable_to_non_nullable
              as CardList4,
      resultOption: resultOption == freezed
          ? _value.resultOption
          : resultOption // ignore: cast_nullable_to_non_nullable
              as Option<Cards>,
    ));
  }
}

/// @nodoc

class _$_TrickState implements _TrickState {
  const _$_TrickState({required this.cards, required this.resultOption});

  @override
  final CardList4 cards;
  @override
  final Option<Cards> resultOption;

  @override
  String toString() {
    return 'TrickState(cards: $cards, resultOption: $resultOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TrickState &&
            const DeepCollectionEquality().equals(other.cards, cards) &&
            const DeepCollectionEquality()
                .equals(other.resultOption, resultOption));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(cards),
      const DeepCollectionEquality().hash(resultOption));

  @JsonKey(ignore: true)
  @override
  _$TrickStateCopyWith<_TrickState> get copyWith =>
      __$TrickStateCopyWithImpl<_TrickState>(this, _$identity);
}

abstract class _TrickState implements TrickState {
  const factory _TrickState(
      {required CardList4 cards,
      required Option<Cards> resultOption}) = _$_TrickState;

  @override
  CardList4 get cards;
  @override
  Option<Cards> get resultOption;
  @override
  @JsonKey(ignore: true)
  _$TrickStateCopyWith<_TrickState> get copyWith =>
      throw _privateConstructorUsedError;
}
