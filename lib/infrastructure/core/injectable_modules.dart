import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:js_card_trick/domain/core/i_card_facade.dart';
import 'package:js_card_trick/infrastructure/core/assets_facade.dart';

final cardFacadeProvider = Provider<ICardFacade>((ref) {
  return AssetCardFacade();
});
