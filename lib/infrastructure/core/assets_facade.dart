import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart' as mat;
import 'package:js_card_trick/domain/core/card_failure.dart';
import 'package:js_card_trick/domain/core/card_image.dart';
import 'package:js_card_trick/domain/core/i_card_facade.dart';

class AssetCardFacade extends ICardFacade {
  static String path = 'assets/images/cards';

  @override
  Future<Either<CardFailure, CardImage>> getCard(String cardName) async {
    final filepath = '$path/$cardName';
    late final mat.Image image;
    try {
      image = mat.Image.asset(filepath);
    } catch (e) {
      return left(const CardFailure.notFound());
    }

    return right(
      CardImage(
        image: image,
      ),
    );
  }
}
