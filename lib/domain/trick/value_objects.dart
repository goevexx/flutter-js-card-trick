import 'package:dartz/dartz.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/domain/core/failures.dart';
import 'package:js_card_trick/domain/core/value_object.dart';
import 'package:js_card_trick/domain/core/value_validators.dart';
import 'package:kt_dart/kt.dart';

class CardList4 extends ValueObject<KtList<Option<Cards>>> {
  @override
  final Either<ValueFailure<KtList<Option<Cards>>>, KtList<Option<Cards>>>
      value;

  factory CardList4.empty() {
    return CardList4([
      none<Cards>(),
      none<Cards>(),
      none<Cards>(),
      none<Cards>(),
    ].toImmutableList());
  }

  factory CardList4(KtList<Option<Cards>> input) {
    return CardList4._(
      validateCards(input),
    );
  }
  const CardList4._(this.value);
}
