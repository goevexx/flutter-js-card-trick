// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cards.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$CardsTearOff {
  const _$CardsTearOff();

  _Cards call(
      {required String filename,
      required Symbols symbol,
      required Values value,
      bool taken = false}) {
    return _Cards(
      filename: filename,
      symbol: symbol,
      value: value,
      taken: taken,
    );
  }
}

/// @nodoc
const $Cards = _$CardsTearOff();

/// @nodoc
mixin _$Cards {
  String get filename => throw _privateConstructorUsedError;
  Symbols get symbol => throw _privateConstructorUsedError;
  Values get value => throw _privateConstructorUsedError;
  bool get taken => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CardsCopyWith<Cards> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CardsCopyWith<$Res> {
  factory $CardsCopyWith(Cards value, $Res Function(Cards) then) =
      _$CardsCopyWithImpl<$Res>;
  $Res call({String filename, Symbols symbol, Values value, bool taken});
}

/// @nodoc
class _$CardsCopyWithImpl<$Res> implements $CardsCopyWith<$Res> {
  _$CardsCopyWithImpl(this._value, this._then);

  final Cards _value;
  // ignore: unused_field
  final $Res Function(Cards) _then;

  @override
  $Res call({
    Object? filename = freezed,
    Object? symbol = freezed,
    Object? value = freezed,
    Object? taken = freezed,
  }) {
    return _then(_value.copyWith(
      filename: filename == freezed
          ? _value.filename
          : filename // ignore: cast_nullable_to_non_nullable
              as String,
      symbol: symbol == freezed
          ? _value.symbol
          : symbol // ignore: cast_nullable_to_non_nullable
              as Symbols,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Values,
      taken: taken == freezed
          ? _value.taken
          : taken // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$CardsCopyWith<$Res> implements $CardsCopyWith<$Res> {
  factory _$CardsCopyWith(_Cards value, $Res Function(_Cards) then) =
      __$CardsCopyWithImpl<$Res>;
  @override
  $Res call({String filename, Symbols symbol, Values value, bool taken});
}

/// @nodoc
class __$CardsCopyWithImpl<$Res> extends _$CardsCopyWithImpl<$Res>
    implements _$CardsCopyWith<$Res> {
  __$CardsCopyWithImpl(_Cards _value, $Res Function(_Cards) _then)
      : super(_value, (v) => _then(v as _Cards));

  @override
  _Cards get _value => super._value as _Cards;

  @override
  $Res call({
    Object? filename = freezed,
    Object? symbol = freezed,
    Object? value = freezed,
    Object? taken = freezed,
  }) {
    return _then(_Cards(
      filename: filename == freezed
          ? _value.filename
          : filename // ignore: cast_nullable_to_non_nullable
              as String,
      symbol: symbol == freezed
          ? _value.symbol
          : symbol // ignore: cast_nullable_to_non_nullable
              as Symbols,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Values,
      taken: taken == freezed
          ? _value.taken
          : taken // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_Cards extends _Cards {
  const _$_Cards(
      {required this.filename,
      required this.symbol,
      required this.value,
      this.taken = false})
      : super._();

  @override
  final String filename;
  @override
  final Symbols symbol;
  @override
  final Values value;
  @JsonKey()
  @override
  final bool taken;

  @override
  String toString() {
    return 'Cards(filename: $filename, symbol: $symbol, value: $value, taken: $taken)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Cards &&
            const DeepCollectionEquality().equals(other.filename, filename) &&
            const DeepCollectionEquality().equals(other.symbol, symbol) &&
            const DeepCollectionEquality().equals(other.value, value) &&
            const DeepCollectionEquality().equals(other.taken, taken));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(filename),
      const DeepCollectionEquality().hash(symbol),
      const DeepCollectionEquality().hash(value),
      const DeepCollectionEquality().hash(taken));

  @JsonKey(ignore: true)
  @override
  _$CardsCopyWith<_Cards> get copyWith =>
      __$CardsCopyWithImpl<_Cards>(this, _$identity);
}

abstract class _Cards extends Cards {
  const factory _Cards(
      {required String filename,
      required Symbols symbol,
      required Values value,
      bool taken}) = _$_Cards;
  const _Cards._() : super._();

  @override
  String get filename;
  @override
  Symbols get symbol;
  @override
  Values get value;
  @override
  bool get taken;
  @override
  @JsonKey(ignore: true)
  _$CardsCopyWith<_Cards> get copyWith => throw _privateConstructorUsedError;
}
