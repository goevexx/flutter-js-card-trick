import 'package:dartz/dartz.dart';
import 'package:js_card_trick/domain/core/failures.dart';

class NotAuthenticatedError extends Error {}

class UnexpectedValueError extends Error {
  final ValueFailure valueFailure;

  UnexpectedValueError(this.valueFailure);

  @override
  String toString() {
    const explanation = 'Encountered a ValueFailure at an unrecoverable point.';
    return Error.safeToString(
        '$explanation Terminating. Failure was: $ValueFailure');
  }
}

class TypeMismatchValueError<Z, T> extends Error {
  final Either<ValueFailure<Z>, T> valueFailure;

  TypeMismatchValueError(this.valueFailure);

  @override
  String toString() {
    const explanation =
        'Encountered a different types error at an unrecoverable point.';
    return Error.safeToString(
        '$explanation Terminating. Different types were: ValueFailure<$Z>, $T');
  }
}

class UnexpectedCoreBehaviourError extends Error {}

class UnexpectedMapBehaviourError extends Error {}
