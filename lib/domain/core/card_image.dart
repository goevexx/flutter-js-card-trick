import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'card_image.freezed.dart';

@freezed
class CardImage with _$CardImage {
  const factory CardImage({
    required Image image,
  }) = _Card;
}
