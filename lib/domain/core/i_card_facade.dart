import 'package:dartz/dartz.dart';
import 'package:js_card_trick/domain/core/card_failure.dart';
import 'package:js_card_trick/domain/core/card_image.dart';

abstract class ICardFacade {
  Future<Either<CardFailure, CardImage>> getCard(String card);
}
