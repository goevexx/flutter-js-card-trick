// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ValueFailureTearOff {
  const _$ValueFailureTearOff();

  InvalidEmail<T> invalidCardCount<T>({required T failedValue}) {
    return InvalidEmail<T>(
      failedValue: failedValue,
    );
  }

  UnselectedCards<T> unselectedCards<T>({required T failedValue}) {
    return UnselectedCards<T>(
      failedValue: failedValue,
    );
  }

  SameCards<T> sameCards<T>({required T failedValue}) {
    return SameCards<T>(
      failedValue: failedValue,
    );
  }
}

/// @nodoc
const $ValueFailure = _$ValueFailureTearOff();

/// @nodoc
mixin _$ValueFailure<T> {
  T get failedValue => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidCardCount,
    required TResult Function(T failedValue) unselectedCards,
    required TResult Function(T failedValue) sameCards,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCardCount,
    TResult Function(T failedValue)? unselectedCards,
    TResult Function(T failedValue)? sameCards,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCardCount,
    TResult Function(T failedValue)? unselectedCards,
    TResult Function(T failedValue)? sameCards,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidCardCount,
    required TResult Function(UnselectedCards<T> value) unselectedCards,
    required TResult Function(SameCards<T> value) sameCards,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidCardCount,
    TResult Function(UnselectedCards<T> value)? unselectedCards,
    TResult Function(SameCards<T> value)? sameCards,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidCardCount,
    TResult Function(UnselectedCards<T> value)? unselectedCards,
    TResult Function(SameCards<T> value)? sameCards,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ValueFailureCopyWith<T, ValueFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValueFailureCopyWith<T, $Res> {
  factory $ValueFailureCopyWith(
          ValueFailure<T> value, $Res Function(ValueFailure<T>) then) =
      _$ValueFailureCopyWithImpl<T, $Res>;
  $Res call({T failedValue});
}

/// @nodoc
class _$ValueFailureCopyWithImpl<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  _$ValueFailureCopyWithImpl(this._value, this._then);

  final ValueFailure<T> _value;
  // ignore: unused_field
  final $Res Function(ValueFailure<T>) _then;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_value.copyWith(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc
abstract class $InvalidEmailCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $InvalidEmailCopyWith(
          InvalidEmail<T> value, $Res Function(InvalidEmail<T>) then) =
      _$InvalidEmailCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$InvalidEmailCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $InvalidEmailCopyWith<T, $Res> {
  _$InvalidEmailCopyWithImpl(
      InvalidEmail<T> _value, $Res Function(InvalidEmail<T>) _then)
      : super(_value, (v) => _then(v as InvalidEmail<T>));

  @override
  InvalidEmail<T> get _value => super._value as InvalidEmail<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(InvalidEmail<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidEmail<T> implements InvalidEmail<T> {
  const _$InvalidEmail({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'ValueFailure<$T>.invalidCardCount(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InvalidEmail<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $InvalidEmailCopyWith<T, InvalidEmail<T>> get copyWith =>
      _$InvalidEmailCopyWithImpl<T, InvalidEmail<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidCardCount,
    required TResult Function(T failedValue) unselectedCards,
    required TResult Function(T failedValue) sameCards,
  }) {
    return invalidCardCount(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCardCount,
    TResult Function(T failedValue)? unselectedCards,
    TResult Function(T failedValue)? sameCards,
  }) {
    return invalidCardCount?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCardCount,
    TResult Function(T failedValue)? unselectedCards,
    TResult Function(T failedValue)? sameCards,
    required TResult orElse(),
  }) {
    if (invalidCardCount != null) {
      return invalidCardCount(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidCardCount,
    required TResult Function(UnselectedCards<T> value) unselectedCards,
    required TResult Function(SameCards<T> value) sameCards,
  }) {
    return invalidCardCount(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidCardCount,
    TResult Function(UnselectedCards<T> value)? unselectedCards,
    TResult Function(SameCards<T> value)? sameCards,
  }) {
    return invalidCardCount?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidCardCount,
    TResult Function(UnselectedCards<T> value)? unselectedCards,
    TResult Function(SameCards<T> value)? sameCards,
    required TResult orElse(),
  }) {
    if (invalidCardCount != null) {
      return invalidCardCount(this);
    }
    return orElse();
  }
}

abstract class InvalidEmail<T> implements ValueFailure<T> {
  const factory InvalidEmail({required T failedValue}) = _$InvalidEmail<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $InvalidEmailCopyWith<T, InvalidEmail<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UnselectedCardsCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $UnselectedCardsCopyWith(
          UnselectedCards<T> value, $Res Function(UnselectedCards<T>) then) =
      _$UnselectedCardsCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$UnselectedCardsCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $UnselectedCardsCopyWith<T, $Res> {
  _$UnselectedCardsCopyWithImpl(
      UnselectedCards<T> _value, $Res Function(UnselectedCards<T>) _then)
      : super(_value, (v) => _then(v as UnselectedCards<T>));

  @override
  UnselectedCards<T> get _value => super._value as UnselectedCards<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(UnselectedCards<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$UnselectedCards<T> implements UnselectedCards<T> {
  const _$UnselectedCards({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'ValueFailure<$T>.unselectedCards(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is UnselectedCards<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $UnselectedCardsCopyWith<T, UnselectedCards<T>> get copyWith =>
      _$UnselectedCardsCopyWithImpl<T, UnselectedCards<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidCardCount,
    required TResult Function(T failedValue) unselectedCards,
    required TResult Function(T failedValue) sameCards,
  }) {
    return unselectedCards(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCardCount,
    TResult Function(T failedValue)? unselectedCards,
    TResult Function(T failedValue)? sameCards,
  }) {
    return unselectedCards?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCardCount,
    TResult Function(T failedValue)? unselectedCards,
    TResult Function(T failedValue)? sameCards,
    required TResult orElse(),
  }) {
    if (unselectedCards != null) {
      return unselectedCards(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidCardCount,
    required TResult Function(UnselectedCards<T> value) unselectedCards,
    required TResult Function(SameCards<T> value) sameCards,
  }) {
    return unselectedCards(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidCardCount,
    TResult Function(UnselectedCards<T> value)? unselectedCards,
    TResult Function(SameCards<T> value)? sameCards,
  }) {
    return unselectedCards?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidCardCount,
    TResult Function(UnselectedCards<T> value)? unselectedCards,
    TResult Function(SameCards<T> value)? sameCards,
    required TResult orElse(),
  }) {
    if (unselectedCards != null) {
      return unselectedCards(this);
    }
    return orElse();
  }
}

abstract class UnselectedCards<T> implements ValueFailure<T> {
  const factory UnselectedCards({required T failedValue}) =
      _$UnselectedCards<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $UnselectedCardsCopyWith<T, UnselectedCards<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SameCardsCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $SameCardsCopyWith(
          SameCards<T> value, $Res Function(SameCards<T>) then) =
      _$SameCardsCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$SameCardsCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $SameCardsCopyWith<T, $Res> {
  _$SameCardsCopyWithImpl(
      SameCards<T> _value, $Res Function(SameCards<T>) _then)
      : super(_value, (v) => _then(v as SameCards<T>));

  @override
  SameCards<T> get _value => super._value as SameCards<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(SameCards<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$SameCards<T> implements SameCards<T> {
  const _$SameCards({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'ValueFailure<$T>.sameCards(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is SameCards<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $SameCardsCopyWith<T, SameCards<T>> get copyWith =>
      _$SameCardsCopyWithImpl<T, SameCards<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidCardCount,
    required TResult Function(T failedValue) unselectedCards,
    required TResult Function(T failedValue) sameCards,
  }) {
    return sameCards(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCardCount,
    TResult Function(T failedValue)? unselectedCards,
    TResult Function(T failedValue)? sameCards,
  }) {
    return sameCards?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCardCount,
    TResult Function(T failedValue)? unselectedCards,
    TResult Function(T failedValue)? sameCards,
    required TResult orElse(),
  }) {
    if (sameCards != null) {
      return sameCards(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidCardCount,
    required TResult Function(UnselectedCards<T> value) unselectedCards,
    required TResult Function(SameCards<T> value) sameCards,
  }) {
    return sameCards(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidCardCount,
    TResult Function(UnselectedCards<T> value)? unselectedCards,
    TResult Function(SameCards<T> value)? sameCards,
  }) {
    return sameCards?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidCardCount,
    TResult Function(UnselectedCards<T> value)? unselectedCards,
    TResult Function(SameCards<T> value)? sameCards,
    required TResult orElse(),
  }) {
    if (sameCards != null) {
      return sameCards(this);
    }
    return orElse();
  }
}

abstract class SameCards<T> implements ValueFailure<T> {
  const factory SameCards({required T failedValue}) = _$SameCards<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $SameCardsCopyWith<T, SameCards<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
