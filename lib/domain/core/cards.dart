import 'package:freezed_annotation/freezed_annotation.dart';

part 'cards.freezed.dart';

enum Symbols { clubs, diamonds, hearts, spades }
enum Values { n2, n3, n4, n5, n6, n7, n8, n9, n10, jack, queen, king, ace }

@freezed
class Cards with _$Cards {
  const factory Cards({
    required String filename,
    required Symbols symbol,
    required Values value,
    @Default(false) bool taken,
  }) = _Cards;

  const Cards._();

  // Returns true, if input card is smaller, else vice versa
  bool compare(Cards card) {
    final valueBigger = value.index > card.value.index;
    if (valueBigger) {
      return true;
    }

    final valueSmaller = card.value.index > value.index;
    if (valueSmaller) {
      return false;
    }

    final symbolBigger = symbol.index > card.symbol.index;
    if (symbolBigger) {
      return true;
    }

    return false;
  }

  factory Cards.c2OfClubs() => Cards(
        filename: '2_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.n2,
      );
  factory Cards.c2OfDiamonds() => Cards(
        filename: '2_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.n2,
      );
  factory Cards.c2OfHearts() => Cards(
        filename: '2_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.n2,
      );
  factory Cards.c2OfSpades() => Cards(
        filename: '2_of_spades.png',
        symbol: Symbols.spades,
        value: Values.n2,
      );
  factory Cards.c3OfClubs() => Cards(
        filename: '3_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.n3,
      );
  factory Cards.c3OfDiamonds() => Cards(
        filename: '3_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.n3,
      );
  factory Cards.c3OfHearts() => Cards(
        filename: '3_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.n3,
      );
  factory Cards.c3OfSpades() => Cards(
        filename: '3_of_spades.png',
        symbol: Symbols.spades,
        value: Values.n3,
      );
  factory Cards.c4OfClubs() => Cards(
        filename: '4_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.n4,
      );
  factory Cards.c4OfDiamonds() => Cards(
        filename: '4_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.n4,
      );
  factory Cards.c4OfHearts() => Cards(
        filename: '4_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.n4,
      );
  factory Cards.c4OfSpades() => Cards(
        filename: '4_of_spades.png',
        symbol: Symbols.spades,
        value: Values.n4,
      );
  factory Cards.c5OfClubs() => Cards(
        filename: '5_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.n5,
      );
  factory Cards.c5OfDiamonds() => Cards(
        filename: '5_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.n5,
      );
  factory Cards.c5OfHearts() => Cards(
        filename: '5_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.n5,
      );
  factory Cards.c5OfSpades() => Cards(
        filename: '5_of_spades.png',
        symbol: Symbols.spades,
        value: Values.n5,
      );
  factory Cards.c6OfClubs() => Cards(
        filename: '6_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.n6,
      );
  factory Cards.c6OfDiamonds() => Cards(
        filename: '6_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.n6,
      );
  factory Cards.c6OfHearts() => Cards(
        filename: '6_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.n6,
      );
  factory Cards.c6OfSpades() => Cards(
        filename: '6_of_spades.png',
        symbol: Symbols.spades,
        value: Values.n6,
      );
  factory Cards.c7OfClubs() => Cards(
        filename: '7_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.n7,
      );
  factory Cards.c7OfDiamonds() => Cards(
        filename: '7_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.n7,
      );
  factory Cards.c7OfHearts() => Cards(
        filename: '7_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.n7,
      );
  factory Cards.c7OfSpades() => Cards(
        filename: '7_of_spades.png',
        symbol: Symbols.spades,
        value: Values.n7,
      );
  factory Cards.c8OfClubs() => Cards(
        filename: '8_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.n8,
      );
  factory Cards.c8OfDiamonds() => Cards(
        filename: '8_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.n8,
      );
  factory Cards.c8OfHearts() => Cards(
        filename: '8_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.n8,
      );
  factory Cards.c8OfSpades() => Cards(
        filename: '8_of_spades.png',
        symbol: Symbols.spades,
        value: Values.n8,
      );
  factory Cards.c9OfClubs() => Cards(
        filename: '9_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.n9,
      );
  factory Cards.c9OfDiamonds() => Cards(
        filename: '9_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.n9,
      );
  factory Cards.c9OfHearts() => Cards(
        filename: '9_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.n9,
      );
  factory Cards.c9OfSpades() => Cards(
        filename: '9_of_spades.png',
        symbol: Symbols.spades,
        value: Values.n9,
      );
  factory Cards.c10OfClubs() => Cards(
        filename: '10_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.n10,
      );
  factory Cards.c10OfDiamonds() => Cards(
        filename: '10_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.n10,
      );
  factory Cards.c10OfHearts() => Cards(
        filename: '10_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.n10,
      );
  factory Cards.c10OfSpades() => Cards(
        filename: '10_of_spades.png',
        symbol: Symbols.spades,
        value: Values.n10,
      );
  factory Cards.cAceOfClubs() => Cards(
        filename: 'ace_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.ace,
      );
  factory Cards.cAceOfDiamonds() => Cards(
        filename: 'ace_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.ace,
      );
  factory Cards.cAceOfHearts() => Cards(
        filename: 'ace_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.ace,
      );
  factory Cards.cAceOfSpades() => Cards(
        filename: 'ace_of_spades.png',
        symbol: Symbols.spades,
        value: Values.ace,
      );
  factory Cards.cBack() => Cards(
        filename: 'backx.png',
        symbol: Symbols.clubs,
        value: Values.n2,
      );
  factory Cards.cJackOfClubs() => Cards(
        filename: 'jack_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.jack,
      );
  factory Cards.cJackOfDiamonds() => Cards(
        filename: 'jack_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.jack,
      );
  factory Cards.cJackOfHearts() => Cards(
        filename: 'jack_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.jack,
      );
  factory Cards.cJackOfSpades() => Cards(
        filename: 'jack_of_spades.png',
        symbol: Symbols.spades,
        value: Values.jack,
      );
  factory Cards.cKingOfClubs() => Cards(
        filename: 'king_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.king,
      );
  factory Cards.cKingOfDiamonds() => Cards(
        filename: 'king_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.king,
      );
  factory Cards.cKingOfHearts() => Cards(
        filename: 'king_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.king,
      );
  factory Cards.cKingOfSpades() => Cards(
        filename: 'king_of_spades.png',
        symbol: Symbols.spades,
        value: Values.king,
      );
  factory Cards.cQueenOfClubs() => Cards(
        filename: 'queen_of_clubs.png',
        symbol: Symbols.clubs,
        value: Values.queen,
      );
  factory Cards.cQueenOfDiamonds() => Cards(
        filename: 'queen_of_diamonds.png',
        symbol: Symbols.diamonds,
        value: Values.queen,
      );
  factory Cards.cQueenOfHearts() => Cards(
        filename: 'queen_of_hearts.png',
        symbol: Symbols.hearts,
        value: Values.queen,
      );
  factory Cards.cQueenOfSpades() => Cards(
        filename: 'queen_of_spades.png',
        symbol: Symbols.spades,
        value: Values.queen,
      );
}
