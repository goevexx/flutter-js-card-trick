import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
class ValueFailure<T> with _$ValueFailure<T> {
  const factory ValueFailure.invalidCardCount({
    required T failedValue,
  }) = InvalidEmail<T>;
  const factory ValueFailure.unselectedCards({
    required T failedValue,
  }) = UnselectedCards<T>;
  const factory ValueFailure.sameCards({
    required T failedValue,
  }) = SameCards<T>;
}
