import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:js_card_trick/domain/core/errors.dart';
import 'package:js_card_trick/domain/core/failures.dart';

@immutable
abstract class ValueObject<T> {
  const ValueObject();
  Either<ValueFailure<dynamic>, T> get value;

  // id = identity - same as writing (right) => right
  T getOrCrash() => value.fold((f) => throw UnexpectedValueError(f), id);

  T getRaw() => value.fold((f) {
        if (f.failedValue is T) {
          return f.failedValue as T;
        } else {
          return throw TypeMismatchValueError(value);
        }
      }, id);

  bool isValid() => value.isRight();

  bool equals(T equalValue) => value.fold(
      (f) => f.failedValue == equalValue, (value) => value == equalValue);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ValueObject<T> && other.value == value;
  }

  @override
  int get hashCode => value.hashCode;

  @override
  String toString() => 'Value($value)';
}
