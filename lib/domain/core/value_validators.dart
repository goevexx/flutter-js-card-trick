import 'package:dartz/dartz.dart';
import 'package:js_card_trick/domain/core/cards.dart';
import 'package:js_card_trick/domain/core/failures.dart';
import 'package:kt_dart/kt.dart';

Either<ValueFailure<KtList<Option<Cards>>>, KtList<Option<Cards>>>
    validateCards(KtList<Option<Cards>> input) {
  if (input.size != 4) {
    return left(ValueFailure.invalidCardCount(failedValue: input));
  }

  if (input.any((cardOption) => cardOption.isNone())) {
    return left(ValueFailure.unselectedCards(failedValue: input));
  } else {
    final Cards? one = input.get(0).getOrElse(() => null as Cards);
    final Cards? two = input.get(1).getOrElse(() => null as Cards);
    final Cards? three = input.get(2).getOrElse(() => null as Cards);
    final Cards? four = input.get(3).getOrElse(() => null as Cards);
    final List<Cards> cards = [one!, two!, three!, four!];
    if (cards.length != cards.toSet().length) {
      return left(ValueFailure.sameCards(failedValue: input));
    }
  }

  // return value
  return right(input);
}
