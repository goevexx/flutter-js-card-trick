# js_card_trick

Watch this video [Javascript Card Trick - Computerphile](https://www.youtube.com/watch?v=rkrjo4IIb1I) to do the trick with a standard deck of 52 cards.

![Preview](js_card_trick.gif)
